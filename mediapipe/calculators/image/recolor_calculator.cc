// Copyright 2019 The MediaPipe Authors.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <vector>

#include "mediapipe/calculators/image/recolor_calculator.pb.h"
#include "mediapipe/framework/calculator_framework.h"
#include "mediapipe/framework/formats/image_frame.h"
#include "mediapipe/framework/formats/image_frame_opencv.h"
#include "mediapipe/framework/port/opencv_core_inc.h"
#include "mediapipe/framework/port/opencv_imgproc_inc.h"
#include "mediapipe/framework/port/ret_check.h"
#include "mediapipe/framework/port/status.h"
#include "mediapipe/util/color.pb.h"

#if !defined(MEDIAPIPE_DISABLE_GPU)
#include "mediapipe/gpu/gl_calculator_helper.h"
#include "mediapipe/gpu/gl_simple_shaders.h"
#include "mediapipe/gpu/shader_util.h"
#endif  //  !MEDIAPIPE_DISABLE_GPU

namespace {
enum { ATTRIB_VERTEX, ATTRIB_TEXTURE_POSITION, NUM_ATTRIBUTES };

constexpr char kImageFrameTag[] = "IMAGE";
constexpr char kMaskCpuTag[] = "MASK";
constexpr char kGpuBufferTag[] = "IMAGE_GPU";
constexpr char kMaskGpuTag[] = "MASK_GPU";
}  // namespace

namespace mediapipe {

// A calculator to recolor a masked area of an image to a specified color.
//
// A mask image is used to specify where to overlay a user defined color.
// The luminance of the input image is used to adjust the blending weight,
// to help preserve image textures.
//
// Inputs:
//   One of the following IMAGE tags:
//   IMAGE: An ImageFrame input image, RGB or RGBA.
//   IMAGE_GPU: A GpuBuffer input image, RGBA.
//   One of the following MASK tags:
//   MASK: An ImageFrame input mask, Gray, RGB or RGBA.
//   MASK_GPU: A GpuBuffer input mask, RGBA.
// Output:
//   One of the following IMAGE tags:
//   IMAGE: An ImageFrame output image.
//   IMAGE_GPU: A GpuBuffer output image.
//
// Options:
//   color_rgb (required): A map of RGB values [0-255].
//   mask_channel (optional): Which channel of mask image is used [RED or ALPHA]
//
// Usage example:
//  node {
//    calculator: "RecolorCalculator"
//    input_stream: "IMAGE_GPU:input_image"
//    input_stream: "MASK_GPU:input_mask"
//    output_stream: "IMAGE_GPU:output_image"
//    node_options: {
//      [mediapipe.RecolorCalculatorOptions] {
//        color { r: 0 g: 0 b: 255 }
//        mask_channel: RED
//      }
//    }
//  }
//
// Note: Cannot mix-match CPU & GPU inputs/outputs.
//       CPU-in & CPU-out <or> GPU-in & GPU-out
class RecolorCalculator : public CalculatorBase {
 public:
  RecolorCalculator() = default;
  ~RecolorCalculator() override = default;

  static mediapipe::Status GetContract(CalculatorContract* cc);

  mediapipe::Status Open(CalculatorContext* cc) override;
  mediapipe::Status Process(CalculatorContext* cc) override;
  mediapipe::Status Close(CalculatorContext* cc) override;

 private:
  mediapipe::Status LoadOptions(CalculatorContext* cc);
  mediapipe::Status InitGpu(CalculatorContext* cc);
  mediapipe::Status RenderGpu(CalculatorContext* cc);
  mediapipe::Status RenderCpu(CalculatorContext* cc);
  void GlRender();

  bool initialized_ = false;
  std::vector<float> color_ = {0,0,0};
  int blendMode_ = 0;
  float alpha_ = 0;
  mediapipe::RecolorCalculatorOptions::MaskChannel mask_channel_;

  bool use_gpu_ = false;
#if !defined(MEDIAPIPE_DISABLE_GPU)
  mediapipe::GlCalculatorHelper gpu_helper_;
  GLuint program_ = 0;
#endif  //  !MEDIAPIPE_DISABLE_GPU
};
REGISTER_CALCULATOR(RecolorCalculator);

// static
mediapipe::Status RecolorCalculator::GetContract(CalculatorContract* cc) {
  RET_CHECK(!cc->Inputs().GetTags().empty());
  RET_CHECK(!cc->Outputs().GetTags().empty());

  bool use_gpu = false;

#if !defined(MEDIAPIPE_DISABLE_GPU)
  if (cc->Inputs().HasTag(kGpuBufferTag)) {
    cc->Inputs().Tag(kGpuBufferTag).Set<mediapipe::GpuBuffer>();
    use_gpu |= true;
  }
#endif  //  !MEDIAPIPE_DISABLE_GPU
  if (cc->Inputs().HasTag(kImageFrameTag)) {
    cc->Inputs().Tag(kImageFrameTag).Set<ImageFrame>();
  }

#if !defined(MEDIAPIPE_DISABLE_GPU)
  if (cc->Inputs().HasTag(kMaskGpuTag)) {
    cc->Inputs().Tag(kMaskGpuTag).Set<mediapipe::GpuBuffer>();
    use_gpu |= true;
  }
#endif  //  !MEDIAPIPE_DISABLE_GPU
  if (cc->Inputs().HasTag(kMaskCpuTag)) {
    cc->Inputs().Tag(kMaskCpuTag).Set<ImageFrame>();
  }

#if !defined(MEDIAPIPE_DISABLE_GPU)
  if (cc->Outputs().HasTag(kGpuBufferTag)) {
    cc->Outputs().Tag(kGpuBufferTag).Set<mediapipe::GpuBuffer>();
    use_gpu |= true;
  }
#endif  //  !MEDIAPIPE_DISABLE_GPU
  if (cc->Outputs().HasTag(kImageFrameTag)) {
    cc->Outputs().Tag(kImageFrameTag).Set<ImageFrame>();
  }

  RET_CHECK(cc->Inputs().HasTag("RGB_VALUE"));
    cc->Inputs().Tag("RGB_VALUE").Set<int[]>();

  RET_CHECK(cc->Inputs().HasTag("BLEND_MODE"));
    cc->Inputs().Tag("BLEND_MODE").Set<int>();

  RET_CHECK(cc->Inputs().HasTag("ALPHA_VALUE"));
    cc->Inputs().Tag("ALPHA_VALUE").Set<float>();

  // Confirm only one of the input streams is present.
  RET_CHECK(cc->Inputs().HasTag(kImageFrameTag) ^
            cc->Inputs().HasTag(kGpuBufferTag));
  // Confirm only one of the output streams is present.
  RET_CHECK(cc->Outputs().HasTag(kImageFrameTag) ^
            cc->Outputs().HasTag(kGpuBufferTag));

  if (use_gpu) {
#if !defined(MEDIAPIPE_DISABLE_GPU)
    MP_RETURN_IF_ERROR(mediapipe::GlCalculatorHelper::UpdateContract(cc));
#endif  //  !MEDIAPIPE_DISABLE_GPU
  }

  return mediapipe::OkStatus();
}

mediapipe::Status RecolorCalculator::Open(CalculatorContext* cc) {
  cc->SetOffset(TimestampDiff(0));

  if (cc->Inputs().HasTag(kGpuBufferTag)) {
    use_gpu_ = true;
#if !defined(MEDIAPIPE_DISABLE_GPU)
    MP_RETURN_IF_ERROR(gpu_helper_.Open(cc));
#endif  //  !MEDIAPIPE_DISABLE_GPU
  }

  MP_RETURN_IF_ERROR(LoadOptions(cc));

  return mediapipe::OkStatus();
}

mediapipe::Status RecolorCalculator::Process(CalculatorContext* cc) {
  if (use_gpu_) {
#if !defined(MEDIAPIPE_DISABLE_GPU)
    MP_RETURN_IF_ERROR(
        gpu_helper_.RunInGlContext([this, &cc]() -> mediapipe::Status {
          if (!initialized_) {
            MP_RETURN_IF_ERROR(InitGpu(cc));
            initialized_ = true;
          }
          MP_RETURN_IF_ERROR(RenderGpu(cc));
          return mediapipe::OkStatus();
        }));
#endif  //  !MEDIAPIPE_DISABLE_GPU
  } else {
    MP_RETURN_IF_ERROR(RenderCpu(cc));
  }
  return mediapipe::OkStatus();
}

mediapipe::Status RecolorCalculator::Close(CalculatorContext* cc) {
#if !defined(MEDIAPIPE_DISABLE_GPU)
  gpu_helper_.RunInGlContext([this] {
    if (program_) glDeleteProgram(program_);
    program_ = 0;
  });
#endif  //  !MEDIAPIPE_DISABLE_GPU

  return mediapipe::OkStatus();
}

mediapipe::Status RecolorCalculator::RenderCpu(CalculatorContext* cc) {
  if (cc->Inputs().Tag(kMaskCpuTag).IsEmpty()) {
    return mediapipe::OkStatus();
  }
  // Get inputs and setup output.
  const auto& input_img = cc->Inputs().Tag(kImageFrameTag).Get<ImageFrame>();
  const auto& mask_img = cc->Inputs().Tag(kMaskCpuTag).Get<ImageFrame>();

  cv::Mat input_mat = formats::MatView(&input_img);
  cv::Mat mask_mat = formats::MatView(&mask_img);

  RET_CHECK(input_mat.channels() == 3);  // RGB only.

  if (mask_mat.channels() > 1) {
    std::vector<cv::Mat> channels;
    cv::split(mask_mat, channels);
    if (mask_channel_ == mediapipe::RecolorCalculatorOptions_MaskChannel_ALPHA)
      mask_mat = channels[3];
    else
      mask_mat = channels[0];
  }
  cv::Mat mask_full;
  cv::resize(mask_mat, mask_full, input_mat.size());

  auto output_img = absl::make_unique<ImageFrame>(
      input_img.Format(), input_mat.cols, input_mat.rows);
  cv::Mat output_mat = mediapipe::formats::MatView(output_img.get());

  // From GPU shader:
  /*
      vec4 weight = texture2D(mask, sample_coordinate);
      vec4 color1 = texture2D(frame, sample_coordinate);
      vec4 color2 = vec4(recolor, 1.0);

      float luminance = dot(color1.rgb, vec3(0.299, 0.587, 0.114));
      float mix_value = weight.MASK_COMPONENT * luminance;

      fragColor = mix(color1, color2, mix_value);
  */
  for (int i = 0; i < output_mat.rows; ++i) {
    for (int j = 0; j < output_mat.cols; ++j) {
      float weight = mask_full.at<uchar>(i, j) * (1.0 / 255.0);
      cv::Vec3f color1 = input_mat.at<cv::Vec3b>(i, j);
      cv::Vec3f color2 = {color_[0], color_[1], color_[2]};

      float luminance =
          (color1[0] * 0.299 + color1[1] * 0.587 + color1[2] * 0.114) / 255;
      float mix_value = weight * luminance;

      cv::Vec3b mix_color = color1 * (1.0 - mix_value) + color2 * mix_value;
      output_mat.at<cv::Vec3b>(i, j) = mix_color;
    }
  }

  cc->Outputs()
      .Tag(kImageFrameTag)
      .Add(output_img.release(), cc->InputTimestamp());

  return mediapipe::OkStatus();
}

mediapipe::Status RecolorCalculator::RenderGpu(CalculatorContext* cc) {
  if (cc->Inputs().Tag(kMaskGpuTag).IsEmpty()) {
    return mediapipe::OkStatus();
  }
#if !defined(MEDIAPIPE_DISABLE_GPU)
  // Get inputs and setup output.
  const Packet& input_packet = cc->Inputs().Tag(kGpuBufferTag).Value();
  const Packet& mask_packet = cc->Inputs().Tag(kMaskGpuTag).Value();

  const auto& input_buffer = input_packet.Get<mediapipe::GpuBuffer>();
  const auto& mask_buffer = mask_packet.Get<mediapipe::GpuBuffer>();

  const auto& rgbValues = cc->Inputs().Tag("RGB_VALUE").Value().Get<int[]>();
  const auto& blendMode = cc->Inputs().Tag("BLEND_MODE").Value().Get<int>();
  const auto& alphaValue = cc->Inputs().Tag("ALPHA_VALUE").Value().Get<float>();

  blendMode_ = blendMode;
  alpha_ = alphaValue / 255.0;

  color_[0] = (rgbValues[0] / 255.0);
  color_[1] = (rgbValues[1] / 255.0);
  color_[2] = (rgbValues[2] / 255.0);

  auto img_tex = gpu_helper_.CreateSourceTexture(input_buffer);
  auto mask_tex = gpu_helper_.CreateSourceTexture(mask_buffer);
  auto dst_tex =
      gpu_helper_.CreateDestinationTexture(img_tex.width(), img_tex.height());

  // Run recolor shader on GPU.
  {
    gpu_helper_.BindFramebuffer(dst_tex);  // GL_TEXTURE0

    glActiveTexture(GL_TEXTURE1);
    glBindTexture(img_tex.target(), img_tex.name());
    glActiveTexture(GL_TEXTURE2);
    glBindTexture(mask_tex.target(), mask_tex.name());

    GlRender();

    glActiveTexture(GL_TEXTURE2);
    glBindTexture(GL_TEXTURE_2D, 0);
    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, 0);
    glFlush();
  }

  // Send result image in GPU packet.
  auto output = dst_tex.GetFrame<mediapipe::GpuBuffer>();
  cc->Outputs().Tag(kGpuBufferTag).Add(output.release(), cc->InputTimestamp());

  // Cleanup
  img_tex.Release();
  mask_tex.Release();
  dst_tex.Release();
#endif  //  !MEDIAPIPE_DISABLE_GPU

  return mediapipe::OkStatus();
}

void RecolorCalculator::GlRender() {
#if !defined(MEDIAPIPE_DISABLE_GPU)
  static const GLfloat square_vertices[] = {
      -1.0f, -1.0f,  // bottom left
      1.0f,  -1.0f,  // bottom right
      -1.0f, 1.0f,   // top left
      1.0f,  1.0f,   // top right
  };
  static const GLfloat texture_vertices[] = {
      0.0f, 0.0f,  // bottom left
      1.0f, 0.0f,  // bottom right
      0.0f, 1.0f,  // top left
      1.0f, 1.0f,  // top right
  };

  // program
  glUseProgram(program_);
  glUniform3f(glGetUniformLocation(program_, "recolor"), color_[0],
                  color_[1], color_[2]);

  glUniform1i(glGetUniformLocation(program_, "blendMode"), blendMode_);

  glUniform1f(glGetUniformLocation(program_, "alpha"), alpha_);

  // vertex storage
  GLuint vbo[2];
  glGenBuffers(2, vbo);
  GLuint vao;
  glGenVertexArrays(1, &vao);
  glBindVertexArray(vao);

  // vbo 0
  glBindBuffer(GL_ARRAY_BUFFER, vbo[0]);
  glBufferData(GL_ARRAY_BUFFER, 4 * 2 * sizeof(GLfloat), square_vertices,
               GL_STATIC_DRAW);
  glEnableVertexAttribArray(ATTRIB_VERTEX);
  glVertexAttribPointer(ATTRIB_VERTEX, 2, GL_FLOAT, 0, 0, nullptr);

  // vbo 1
  glBindBuffer(GL_ARRAY_BUFFER, vbo[1]);
  glBufferData(GL_ARRAY_BUFFER, 4 * 2 * sizeof(GLfloat), texture_vertices,
               GL_STATIC_DRAW);
  glEnableVertexAttribArray(ATTRIB_TEXTURE_POSITION);
  glVertexAttribPointer(ATTRIB_TEXTURE_POSITION, 2, GL_FLOAT, 0, 0, nullptr);

  // draw
  glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

  // cleanup
  glDisableVertexAttribArray(ATTRIB_VERTEX);
  glDisableVertexAttribArray(ATTRIB_TEXTURE_POSITION);
  glBindBuffer(GL_ARRAY_BUFFER, 0);
  glBindVertexArray(0);
  glDeleteVertexArrays(1, &vao);
  glDeleteBuffers(2, vbo);
#endif  //  !MEDIAPIPE_DISABLE_GPU
}

mediapipe::Status RecolorCalculator::LoadOptions(CalculatorContext* cc) {
  const auto& options = cc->Options<mediapipe::RecolorCalculatorOptions>();

  mask_channel_ = options.mask_channel();

  if (!options.has_color()) RET_CHECK_FAIL() << "Missing color option.";

  color_[0] = (options.color().r() / 255.0);
  color_[1] = (options.color().g() / 255.0);
  color_[2] = (options.color().b() / 255.0);

  blendMode_ = options.blend_mode();

  return mediapipe::OkStatus();
}

mediapipe::Status RecolorCalculator::InitGpu(CalculatorContext* cc) {
#if !defined(MEDIAPIPE_DISABLE_GPU)
  const GLint attr_location[NUM_ATTRIBUTES] = {
      ATTRIB_VERTEX,
      ATTRIB_TEXTURE_POSITION,
  };
  const GLchar* attr_name[NUM_ATTRIBUTES] = {
      "position",
      "texture_coordinate",
  };

  std::string mask_component;
  switch (mask_channel_) {
    case mediapipe::RecolorCalculatorOptions_MaskChannel_UNKNOWN:
    case mediapipe::RecolorCalculatorOptions_MaskChannel_RED:
      mask_component = "r";
      break;
    case mediapipe::RecolorCalculatorOptions_MaskChannel_ALPHA:
      mask_component = "a";
      break;
  }

  // A shader to blend a color onto an image where the mask > 0.
  // The blending is based on the input image luminosity.
  const std::string frag_src = R"(
  #if __VERSION__ < 130
    #define in varying
  #endif  // __VERSION__ < 130

  #ifdef GL_ES
    #define fragColor gl_FragColor
    precision highp float;
  #else
    #define lowp
    #define mediump
    #define highp
    #define texture2D texture
    out vec4 fragColor;
  #endif  // defined(GL_ES)

    #define MASK_COMPONENT )" + mask_component +
                               R"(

    in vec2 sample_coordinate;
    uniform sampler2D frame;
    uniform sampler2D mask;
    uniform int blendMode;
    uniform float alpha;
    uniform vec3 recolor;

    float hueToRgb(float t1, float t2, float hue) {
      if (hue < 0.0) hue += 6.0;
      if (hue >= 6.0) hue -= 6.0;
      if (hue < 1.0) return (t2 - t1) * hue + t1;
      else if(hue < 3.0) return t2;
      else if(hue < 4.0) return (t2 - t1) * (4.0 - hue) + t1;
      else return t1;
    }

    void main() {

      // DST-OVER blend mode (to blend the input frame and input lipstick colour)
      // [Da + Sa·(1 – Da), Dc + Sc·(1 – Da)] [Alpha, Color]
      // Da is the destination alpha channel
      // Dc is the destination RGB color
      // Sa is the source alpha channel
      // Sc is the source RGB color
      // link: https://docs.microsoft.com/en-us/xamarin/xamarin-forms/user-interface/graphics/skiasharp/effects/blend-modes/porter-duff
      // link: https://developer.android.com/reference/android/graphics/PorterDuff.Mode

      // weight is the mask input frame
      // dstColor is the DST (Video input frame)
      // srcColor is the lipstick color (HexCode).
      // lipsColor = B(dstColor, srcColor) B is the blend option

      vec4 weight = texture2D(mask, sample_coordinate);
      vec4 dstColor = texture2D(frame, sample_coordinate);
      vec4 srcColor = vec4(recolor, 1.0);
      vec4 lipsColor = vec4(0.0, 0.0, 0.0, 0.0);
      float alphaSrc = weight.MASK_COMPONENT;
      float alphaLips = alphaSrc + dstColor.w - (alphaSrc * dstColor.w);
      alphaSrc = 1.0;

      // Alpha value for the lipstick colour
      float lipstickAlpha = alpha;

      /*
       OVERLAY -> 0
       HARDLIGHT -> 1
       DARKEN -> 2
       COLOR -> 3
       MULTIPLY -> 4
       CUSTOM MATTE -> 5
       SOFTLIGHT -> 6
       SRC-OVER -> 7
      */

      // OVERLAY blend mode
      if(blendMode == 0){
        lipsColor = vec4((2.0 * dstColor.x <= 1.0)
                              ? (2.0 * srcColor.x * dstColor.x)
                              : srcColor.x + (2.0 * dstColor.x - 1.0) - (srcColor.x * (2.0 * dstColor.x - 1.0)),
                         (2.0 * dstColor.y <= 1.0)
                              ? (2.0 * srcColor.y * dstColor.y)
                              : srcColor.y + (2.0 * dstColor.y - 1.0) - (srcColor.y * (2.0 * dstColor.y - 1.0)),
                         (2.0 * dstColor.z <= 1.0)
                              ? (2.0 * srcColor.z * dstColor.z)
                              : srcColor.z + (2.0 * dstColor.z - 1.0) - (srcColor.z * (2.0 * dstColor.z - 1.0)),
                         alphaLips);
      }

      // HARDLIGHT blend mode
      else if(blendMode == 1){
        lipsColor = vec4((2.0 * srcColor.x <= 1.0)
                              ? (2.0 * srcColor.x * dstColor.x)
                              : dstColor.x  + (2.0 * srcColor.x - 1.0) - (dstColor.x * (2.0 * srcColor.x - 1.0)),
                         (2.0 * srcColor.y <= 1.0)
                              ? (2.0 * srcColor.y * dstColor.y)
                              : dstColor.y  + (2.0 * srcColor.y - 1.0) - (dstColor.y * (2.0 * srcColor.y - 1.0)),
                         (2.0 * srcColor.z <= 1.0)
                              ? (2.0 * srcColor.z * dstColor.z)
                              : dstColor.z  + (2.0 * srcColor.z - 1.0) - (dstColor.z * (2.0 * srcColor.z - 1.0)),
                         alphaLips);
      }

      // DARKEN blend mode
      else if(blendMode == 2){
       lipsColor = vec4(min(srcColor.x, dstColor.x), min(srcColor.y, dstColor.y), min(srcColor.z, dstColor.z),
                        alphaLips);
      }

      // COLOR blend mode
      else if(blendMode == 3) {

        // rgb to hsl
        // SRC color calculation
        float cMax = max(srcColor.x, max(srcColor.y, srcColor.z));
        float cMin = min(srcColor.x, min(srcColor.y, srcColor.z));

        float hSrc = 0.0;
        if(cMax == srcColor.x) hSrc = (srcColor.y - srcColor.z) / (cMax - cMin);
        else if(cMax == srcColor.y) hSrc = 2.0 + (srcColor.z - srcColor.x) / (cMax - cMin);
        else if(cMax == srcColor.z) hSrc = 4.0 + (srcColor.x - srcColor.y) / (cMax - cMin);
        hSrc = hSrc * 60.0;
        if(hSrc < 0.0) hSrc = hSrc + 360.0;

        float lSrc = (cMax + cMin) / 2.0;

        float satSrc = 0.0;
        if(cMin == cMax) satSrc = 0.0;
        else {
            if(lSrc < 0.5) satSrc = (cMax - cMin) / (cMax + cMin);
            else satSrc = (cMax - cMin) / (2.0 - cMax + cMin);
        }

        // DST color calculation
        float dMax = max(dstColor.x, max(dstColor.y, dstColor.z));
        float dMin = min(dstColor.x, min(dstColor.y, dstColor.z));
        float lDst = (dMax + dMin) / 2.0;


        // hsl to rgb
        float hLips = hSrc;
        float sLips = satSrc;
        float lLips = lDst;

        float rValue = 0.0, gValue = 0.0, bValue = 0.0, t1 = 0.0, t2 = 0.0;
        hLips = hLips / 60.0;

        if(lLips <= 0.5) t2 = lLips * (sLips + 1.0);
        else t2 = lLips + sLips - (lLips * sLips);
        t1 = lLips * 2.0 - t2;

        rValue = hueToRgb(t1, t2, hLips + 2.0);
        bValue = hueToRgb(t1, t2, hLips);
        bValue = hueToRgb(t1, t2, hLips - 2.0);

        lipsColor = vec4(rValue, gValue, bValue, alphaLips);
      }

      // MULTIPLY blend mode
      else if(blendMode == 4) {
       lipsColor = vec4(srcColor.x * dstColor.x,
                        srcColor.y * dstColor.y,
                        srcColor.z * dstColor.z,
                        alphaLips);
      }

           // CUSTOM MATTE blend mode
           else if(blendMode == 5){
              float Cx = (1.0 - dstColor.w) * srcColor.x + (1.0 - alphaSrc) * dstColor.x + min(srcColor.x, dstColor.x);
              float Cy = (1.0 - dstColor.w) * srcColor.y + (1.0 - alphaSrc) * dstColor.y + min(srcColor.y, dstColor.y);
              float Cz = (1.0 - dstColor.w) * srcColor.z + (1.0 - alphaSrc) * dstColor.z + min(srcColor.z, dstColor.z);
              vec4 darkenAppliedColor = vec4(Cx, Cy, Cz,
                          alphaLips);
              darkenAppliedColor = mix(dstColor, darkenAppliedColor, lipstickAlpha);

              lipsColor = vec4((2.0 * srcColor.x <= 1.0)
                                    ? (2.0 * srcColor.x * darkenAppliedColor.x)
                                    : darkenAppliedColor.x  + (2.0 * srcColor.x - 1.0) - (darkenAppliedColor.x * (2.0 * srcColor.x - 1.0)),
                               (2.0 * srcColor.y <= 1.0)
                                    ? (2.0 * srcColor.y * darkenAppliedColor.y)
                                    : darkenAppliedColor.y  + (2.0 * srcColor.y - 1.0) - (darkenAppliedColor.y * (2.0 * srcColor.y - 1.0)),
                               (2.0 * srcColor.z <= 1.0)
                                    ? (2.0 * srcColor.z * darkenAppliedColor.z)
                                    : darkenAppliedColor.z  + (2.0 * srcColor.z - 1.0) - (darkenAppliedColor.z * (2.0 * srcColor.z - 1.0)),
                               alphaLips);
           }

      // SOFTLIGHT blend mode
      else if(blendMode == 6){

       if(srcColor.x <= 0.5) {
        float Cx = dstColor.x - (1.0 - 2.0 * srcColor.x) * dstColor.x * (1.0 - dstColor.x);
        float Cy = dstColor.y - (1.0 - 2.0 * srcColor.y) * dstColor.y * (1.0 - dstColor.y);
        float Cz = dstColor.z - (1.0 - 2.0 * srcColor.z) * dstColor.z * (1.0 - dstColor.z);

        lipsColor = vec4(Cx, Cy, Cz, alphaLips);
       }

       else {
        float dDstX = dstColor.x <= 0.25
                        ? ((16.0 * dstColor.x - 12.0) * dstColor.x + 4.0) * dstColor.x
                        : sqrt(dstColor.x);
        float Cx = dstColor.x + (2.0 * srcColor.x - 1.0) * (dDstX - dstColor.x);

        float dDstY = dstColor.y <= 0.25
                                ? ((16.0 * dstColor.y - 12.0) * dstColor.y + 4.0) * dstColor.y
                                : sqrt(dstColor.y);
        float Cy = dstColor.y + (2.0 * srcColor.y - 1.0) * (dDstY - dstColor.y);

        float dDstZ = dstColor.z <= 0.25
                                ? ((16.0 * dstColor.z - 12.0) * dstColor.z + 4.0) * dstColor.z
                                : sqrt(dstColor.z);
        float Cz = dstColor.z + (2.0 * srcColor.z - 1.0) * (dDstZ - dstColor.z);

        lipsColor = vec4(Cx, Cy, Cz, alphaLips);
       }
      }

      // SRC-OVER blend mode
      else if(blendMode == 7) {
       lipsColor = vec4(srcColor.x, srcColor.y, srcColor.z, alphaLips);
      }




      if(lipsColor.x + lipsColor.y + lipsColor.z + lipsColor.w == 0.0) fragColor = dstColor;
      else if(weight.MASK_COMPONENT == 0.0) fragColor = dstColor;
      else {
         float mix_value = weight.MASK_COMPONENT * lipstickAlpha;
         fragColor = mix(dstColor, lipsColor, mix_value);
      }
    }
  )";

  // LOG(INFO) << "Recolor Calculator Runner " <<  blendMode_ << "\n";

   // shader program and params
  mediapipe::GlhCreateProgram(mediapipe::kBasicVertexShader, frag_src.c_str(),
                              NUM_ATTRIBUTES, &attr_name[0], attr_location,
                              &program_);
  RET_CHECK(program_) << "Problem initializing the program.";
  glUseProgram(program_);
  glUniform1i(glGetUniformLocation(program_, "frame"), 1);
  glUniform1i(glGetUniformLocation(program_, "mask"), 2);
  glUniform1i(glGetUniformLocation(program_, "blendMode"), blendMode_);
  glUniform1f(glGetUniformLocation(program_, "alpha"), alpha_);
  glUniform3f(glGetUniformLocation(program_, "recolor"), color_[0],
              color_[1], color_[2]);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
#endif  //  !MEDIAPIPE_DISABLE_GPU

  return mediapipe::OkStatus();
}

}  // namespace mediapipe
