#include "mediapipe/framework/calculator_framework.h"
#include "mediapipe/framework/formats/image_format.pb.h"
#include "mediapipe/framework/formats/image_frame.h"

namespace mediapipe {

class CoordinatesToRecolorCalculator: public CalculatorBase {
 public:
  CoordinatesToRecolorCalculator() = default;
  ~CoordinatesToRecolorCalculator() override = default;

  static ::mediapipe::Status GetContract(CalculatorContract* cc);

  ::mediapipe::Status Open(CalculatorContext* cc) override;
  ::mediapipe::Status Process(CalculatorContext* cc) override;
};
REGISTER_CALCULATOR(CoordinatesToRecolorCalculator);

::mediapipe::Status CoordinatesToRecolorCalculator::GetContract (CalculatorContract *cc){

    cc->Inputs().Tag("NORM_LANDMARKS").Set<ImageFrame>();
    cc->Inputs().Tag("IMAGE").Set<ImageFrame>();
    cc->Outputs().Tag("IMAGE").Set<ImageFrame>();

    return ::mediapipe::OkStatus();
}

::mediapipe::Status CoordinatesToRecolorCalculator::Open(CalculatorContext* cc) {
    return ::mediapipe::OkStatus();
}

::mediapipe::Status CoordinatesToRecolorCalculator::Process(CalculatorContext* cc) {

    const auto& face_landmarks = cc->Inputs().Tag("NORM_LANDMARKS").Get<ImageFrame>();
    const auto& input_img = cc->Inputs().Tag("IMAGE").Get<ImageFrame>();

//    points = []
//    points.append((0, 0))
//    points.append((1, 1))
//
//    glLineWidth(2.0)
//    glBegin(GL_LINES)
//    glColor3f(0.0, 1.0, 0.0)
//    for pt1 in points:
//        for pt2 in points:
//            glVertex2f(*pt1)
//            glVertex2f(*pt2)
//    glEnd()

    std::unique_ptr<ImageFrame> output_frame(
        new ImageFrame(input_img.Format(), input_img.Width(), input_img.Height()));

    // output_frame is the IMAGE_GPU after adding the overlay
    cc->Outputs().Tag("IMAGE").Add(output_frame.release(), cc->InputTimestamp());

    return ::mediapipe::OkStatus();

}

REGISTER_CALCULATOR(::mediapipe::CoordinatesToRecolorCalculator);

}