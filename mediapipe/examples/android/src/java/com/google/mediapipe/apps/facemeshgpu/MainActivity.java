// Copyright 2019 The MediaPipe Authors.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package com.google.mediapipe.apps.facemeshgpu;

import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.view.View;
import android.graphics.Color;
import com.google.mediapipe.components.FrameProcessor;
import com.google.mediapipe.formats.proto.LandmarkProto.NormalizedLandmark;
import com.google.mediapipe.formats.proto.LandmarkProto.NormalizedLandmarkList;
import com.google.mediapipe.framework.AndroidPacketCreator;
import com.google.mediapipe.framework.Packet;
import com.google.mediapipe.framework.PacketGetter;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;

/** Main activity of MediaPipe face mesh app. */
public class MainActivity extends com.google.mediapipe.apps.basic.MainActivity {
  private static final String TAG = "MainActivity";

  private static final String INPUT_NUM_FACES_SIDE_PACKET_NAME = "num_faces";
  private static final String OUTPUT_LANDMARKS_STREAM_NAME = "multi_face_landmarks";
  // Max number of faces to detect/process.
  private static final int NUM_FACES = 1;

  private final List<String> colors = new ArrayList<>();
  private final List<String> blendModes = new ArrayList<>();
  private int selectedColorInd = 0;
  private int blendMode = 0;
  private int alpha = 140;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    AndroidPacketCreator packetCreator = processor.getPacketCreator();
    Map<String, Packet> inputSidePackets = new HashMap<>();
    inputSidePackets.put(INPUT_NUM_FACES_SIDE_PACKET_NAME, packetCreator.createInt32(NUM_FACES));
    processor.setInputSidePackets(inputSidePackets);

    addColors();
    addBlendModes();

    LipstickHandler lipstickHandler = new LipstickHandler();
    processor.setOnWillAddFrameListener(lipstickHandler);

    Button changeColor = findViewById(R.id.changeColor);
    Button changeBlendMode = findViewById(R.id.changeBlendMode);
    Button changeAlpha = findViewById(R.id.changeAlpha);

    changeColor.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        selectedColorInd = (selectedColorInd + 1) % colors.size();
        changeColor.setText("Color: " + colors.get(selectedColorInd));
      }
    });

    changeBlendMode.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        blendMode = (blendMode + 1) % blendModes.size();;
        changeBlendMode.setText("Blend Mode: " + blendModes.get(blendMode));
      }
    });

    changeAlpha.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        alpha = (alpha + 1) % 256;
        changeAlpha.setText("Alpha: " + alpha);
      }
    });

    // To show verbose logging, run:
    // adb shell setprop log.tag.MainActivity VERBOSE
    if (Log.isLoggable(TAG, Log.VERBOSE)) {
      processor.addPacketCallback(
        OUTPUT_LANDMARKS_STREAM_NAME,
        (packet) -> {
          Log.v(TAG, "Received multi face landmarks packet.");
          List<NormalizedLandmarkList> multiFaceLandmarks =
              PacketGetter.getProtoVector(packet, NormalizedLandmarkList.parser());
          Log.v(
              TAG,
              "[TS:"
                  + packet.getTimestamp()
                  + "] "
                  + getMultiFaceLandmarksDebugString(multiFaceLandmarks));
        });
    }
  }

  private void addColors() {

//    colors.add("#df477a");
//    colors.add("#ff339a");
//    colors.add("#e21d79");
//    colors.add("#e01a58");
//    colors.add("#dd0747");
//    colors.add("#ff4e7a");
//    colors.add("#f17da4");

    colors.add("#bd1d5e");
    colors.add("#934b52");
    colors.add("#c3286a");
    colors.add("#a7192f");
    colors.add("#df477a");
    colors.add("#ff339a");
    colors.add("#e21d79");
    colors.add("#e01a58");
    colors.add("#dd0747");
    colors.add("#ff4e7a");
    colors.add("#f17da4");
    colors.add("#d24259");
    colors.add("#f0a6b3");
    colors.add("#ff73df");
    colors.add("#d290e7");
    colors.add("#ffaeff");
    colors.add("#791555");
    colors.add("#8b1255");
  }

  private void addBlendModes() {
    blendModes.add("OVERLAY");
    blendModes.add("HARDLIGHT");
    blendModes.add("DARKEN");
    blendModes.add("COLOR");
    blendModes.add("MULTIPLY");
    blendModes.add("CUSTOM");
    blendModes.add("SOFTLIGHT");
    blendModes.add("SRC_OVER");
  }

  private int getAlpha(int alpha) {
    if (alpha > 80) {
      alpha = (int) (alpha * 0.9f + 0.5f);
    }
    return alpha;
  }

  @Override
  protected int getContentViewLayoutResId() {
    return R.layout.face_mesh_activity_main;
  }

  private static String getMultiFaceLandmarksDebugString(
      List<NormalizedLandmarkList> multiFaceLandmarks) {
    if (multiFaceLandmarks.isEmpty()) {
      return "No face landmarks";
    }
    String multiFaceLandmarksStr = "Number of faces detected: " + multiFaceLandmarks.size() + "\n";
    int faceIndex = 0;
    for (NormalizedLandmarkList landmarks : multiFaceLandmarks) {
      multiFaceLandmarksStr +=
          "\t#Face landmarks for face[" + faceIndex + "]: " + landmarks.getLandmarkCount() + "\n";
      int landmarkIndex = 0;
      for (NormalizedLandmark landmark : landmarks.getLandmarkList()) {
        multiFaceLandmarksStr +=
            "\t\tLandmark ["
                + landmarkIndex
                + "]: ("
                + landmark.getX()
                + ", "
                + landmark.getY()
                + ", "
                + landmark.getZ()
                + ")\n";
        ++landmarkIndex;
      }
      ++faceIndex;
    }
    return multiFaceLandmarksStr;
  }

  private class LipstickHandler implements FrameProcessor.OnWillAddFrameListener{
    @Override
    public void onWillAddFrame(long timestamp){

      int color = Color.parseColor(colors.get(selectedColorInd));
      int r = (color >> 16) & 0xFF;
      int g = (color >> 8) & 0xFF;
      int b = (color) & 0xFF;
      int[] rgbValue = new int[] {r, g, b};
      Packet rgb_packet = processor.getPacketCreator().createInt32Array(rgbValue);
      Packet blendMode_packet = processor.getPacketCreator().createInt32(blendMode);
      Packet alpha_packet = processor.getPacketCreator().createFloat32(getAlpha(getAlpha(alpha)));

      processor.getGraph().addConsumablePacketToInputStream("rgb_values",
              rgb_packet,  timestamp);
      processor.getGraph().addConsumablePacketToInputStream("blend_mode",
              blendMode_packet,  timestamp);
      processor.getGraph().addConsumablePacketToInputStream("alpha",
              alpha_packet,  timestamp);

      rgb_packet.release();
      blendMode_packet.release();
      alpha_packet.release();
    }
  }
}
